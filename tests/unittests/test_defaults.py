import unittest
import logger
import defaults


class TestDefaults(unittest.TestCase):

    def test_defaults(self):
        self.assertIsInstance(defaults.CONFIG_PATHS, list)
        self.assertGreaterEqual(len(defaults.CONFIG_PATHS), 1)

        self.assertIsInstance(defaults.LOG_DATE_FORMAT, str)

        self.assertIsInstance(defaults.LOG_PATH, str)

        self.assertIsInstance(defaults.LOG_LEVEL, int)
        self.assertIn(defaults.LOG_LEVEL, range(
            logger.Logger.MIN_LOG_LEVEL, logger.Logger.MAX_LOG_LEVEL))

        self.assertIsInstance(defaults.LOG_LEVEL_DEBUG, int)
        self.assertIn(defaults.LOG_LEVEL_DEBUG, range(
            logger.Logger.MIN_LOG_LEVEL, logger.Logger.MAX_LOG_LEVEL))

        self.assertIsInstance(defaults.LOG_FORMAT, str)

        self.assertIsInstance(defaults.LOG_FORMAT_DEBUG, str)
