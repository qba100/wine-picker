import unittest
from mock import patch
from extended_configparser import ExtendedRawConfigParser


class TestExtendedRawConfigParser(unittest.TestCase):

    def setUp(self):
        pass

    @patch("extended_configparser.logging")
    def test_getlist(self, mock_logging):
        good_config = '''
        [
        "value1",
        "value2"
        ]
        '''
        good_result = ["value1", "value2"]

        bad_config = '''
        [
        "value1,
        "value2"
        ]
        '''
        empty_config = ''

        section = "section"
        key = "key"
        message_err = "Cannot parse json value from config section: \"{}\", key: \"{}\"".format(
            section, key)

        config = ExtendedRawConfigParser()

        with patch("extended_configparser.ExtendedRawConfigParser.get", return_value=good_config):
            result = config.getlist(section, key)
            self.assertListEqual(result, good_result)

        with patch("extended_configparser.ExtendedRawConfigParser.get", return_value=bad_config):
            with self.assertRaisesRegex(Exception, message_err):
                result = config.getlist(section, key)

        with patch("extended_configparser.ExtendedRawConfigParser.get", return_value=empty_config):
            with self.assertRaisesRegex(Exception, message_err):
                result = config.getlist(section, key)
