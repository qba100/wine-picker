import unittest
from mock import patch
from extended_git import ExtendedGitCmd


class MockGitCmd:

    @staticmethod
    def ls_remote(url_repo):
        if url_repo == "https://good.com":
            return '''\
06531b1d9898ba3ac3b7c69d6192682202606f8f	HEAD
06531b1d9898ba3ac3b7c69d6192682202606f8f	refs/heads/master
bab51fbb231e1e05990ff2bc4632cc915ffd9014	refs/heads/oldstable
6b42eae19f3bd9a4f4412fcb21c2f46a4c95bbfd	refs/heads/stable
7d67939212f21e862af34fcc73a2aa4a3903f9cd	refs/tags/wine-0.0.2
2c25c3e9442c69bd2402f94f264f0aafa58b00e0	refs/tags/wine-0.0.2^{}
daa70e9faf6317f145f96db94a94a50ae870475f	refs/tags/wine-0.0.3
066d1e09a45147f50de460d69cca30412daea7f4	refs/tags/wine-0.0.3^{}
c047a0023bc928e0277b693ae47ddea85f50c665	refs/tags/wine-0.1.0
121bd98c16b83ac339eed641b95216869ff3f67c	refs/tags/wine-0.1.0^{}
3ad561cd8eec161c001455b776d57165e492efc9	refs/tags/wine-3.0-rc5
bd41696680a15f399558b8635525537337f997d0	refs/tags/wine-3.0-rc5^{}'''
        else:
            raise Exception("Wrong url")


class TestExtendedGitCmd(unittest.TestCase):

    @patch("git.cmd.Git", MockGitCmd)
    def test_ls_remote(self):

        results = {
            'HEAD': '06531b1d9898ba3ac3b7c69d6192682202606f8f',
            'refs/heads/master': '06531b1d9898ba3ac3b7c69d6192682202606f8f',
            'refs/heads/oldstable': 'bab51fbb231e1e05990ff2bc4632cc915ffd9014',
            'refs/heads/stable': '6b42eae19f3bd9a4f4412fcb21c2f46a4c95bbfd',
            'refs/tags/wine-0.0.2': '7d67939212f21e862af34fcc73a2aa4a3903f9cd',
            'refs/tags/wine-0.0.2^{}': '2c25c3e9442c69bd2402f94f264f0aafa58b00e0',
            'refs/tags/wine-0.0.3': 'daa70e9faf6317f145f96db94a94a50ae870475f',
            'refs/tags/wine-0.0.3^{}': '066d1e09a45147f50de460d69cca30412daea7f4',
            'refs/tags/wine-0.1.0': 'c047a0023bc928e0277b693ae47ddea85f50c665',
            'refs/tags/wine-0.1.0^{}': '121bd98c16b83ac339eed641b95216869ff3f67c',
            'refs/tags/wine-3.0-rc5': '3ad561cd8eec161c001455b776d57165e492efc9',
            'refs/tags/wine-3.0-rc5^{}': 'bd41696680a15f399558b8635525537337f997d0'
        }

        good_url = "https://good.com"

        dict_with_tags = ExtendedGitCmd.ls_remote(good_url)
        self.assertIsInstance(dict_with_tags, dict)
        self.assertEqual(dict_with_tags, results)
