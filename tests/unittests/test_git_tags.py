import unittest
from mock import patch
from wine_git_tags import WineGitTags


def mock_ls_remote(url_repo):

    if url_repo == "https://good.com" or "https://good.com" in url_repo:
        return {
            'HEAD': '06531b1d9898ba3ac3b7c69d6192682202606f8f',
            'refs/heads/master': '06531b1d9898ba3ac3b7c69d6192682202606f8f',
            'refs/heads/oldstable': 'bab51fbb231e1e05990ff2bc4632cc915ffd9014',
            'refs/heads/stable': '6b42eae19f3bd9a4f4412fcb21c2f46a4c95bbfd',
            'refs/tags/wine-0.0.2': '7d67939212f21e862af34fcc73a2aa4a3903f9cd',
            'refs/tags/wine-0.0.2^{}': '2c25c3e9442c69bd2402f94f264f0aafa58b00e0',
            'refs/tags/wine-0.0.3': 'daa70e9faf6317f145f96db94a94a50ae870475f',
            'refs/tags/wine-0.0.3^{}': '066d1e09a45147f50de460d69cca30412daea7f4',
            'refs/tags/wine-0.1.0': 'c047a0023bc928e0277b693ae47ddea85f50c665',
            'refs/tags/wine-0.1.0^{}': '121bd98c16b83ac339eed641b95216869ff3f67c',
            'refs/tags/wine-3.0-rc5': '3ad561cd8eec161c001455b776d57165e492efc9',
            'refs/tags/wine-3.0-rc5^{}': 'bd41696680a15f399558b8635525537337f997d0',
        }
    else:
        raise Exception("Wrong url")


class TestGitTags(unittest.TestCase):

    @patch("extended_git.ExtendedGitCmd.ls_remote", mock_ls_remote)
    def test_non_valid_url(self):
        wrong_url = "wrong_url_repo"
        good_url = "https://good.com"
        list_wrong_urls = [wrong_url, wrong_url]
        list_mixed_url = [wrong_url, good_url]

        with self.assertRaisesRegex(Exception,
                                    '{} is not valid url'.format(" ".join([wrong_url]))):
            WineGitTags(wrong_url)

        with self.assertRaisesRegex(Exception,
                                    '{} is not valid url'.format(" ".join(list_wrong_urls))):
            WineGitTags(list_wrong_urls)

        try:
            WineGitTags(good_url)
            WineGitTags(list_mixed_url)
        except ExceptionType:
            self.fail("WineGitTags raised ExceptionType unexpectedly!")

    @patch("extended_git.ExtendedGitCmd.ls_remote", mock_ls_remote)
    def test_get_tags(self):
        results = ['wine-0.0.2', 'wine-0.0.3', 'wine-0.1.0', 'wine-3.0-rc5']
        wrong_url = "https://bad.com"
        good_url = "https://good.com"
        list_wrong_urls = [wrong_url, wrong_url]
        list_mixed_url = [wrong_url, good_url]
        empty_url = ""

        self.wine_tags = WineGitTags(good_url)
        self.assertIsInstance(self.wine_tags.get_tags(), list)
        self.assertEqual(set(results), set(self.wine_tags.get_tags()))

        self.wine_tags = WineGitTags(list_mixed_url)
        self.assertIsInstance(self.wine_tags.get_tags(), list)
        self.assertEqual(set(results), set(self.wine_tags.get_tags()))

        with self.assertRaisesRegex(Exception, "Download wine git tags failed"):
            self.wine_tags = WineGitTags(wrong_url)
            self.wine_tags.get_tags()

        with self.assertRaisesRegex(Exception, "Download wine git tags failed"):
            self.wine_tags = WineGitTags(list_wrong_urls)
            self.wine_tags.get_tags()

        with self.assertRaisesRegex(
                Exception, "Wine git repo not set"):
            self.wine_tags = WineGitTags(empty_url)
            self.wine_tags.get_tags()
