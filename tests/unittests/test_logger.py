import unittest
import os
import logger
import defaults
from mock import patch, Mock


class TestLogger(unittest.TestCase):

    def test_const(self):
        self.assertIsInstance(logger.Logger.MAX_LOG_LEVEL, int)
        self.assertIsInstance(logger.Logger.MIN_LOG_LEVEL, int)

    def __mock_get_level(section, option):
        if section == "LOGS":
            if option == "LOG_LEVEL":
                return 2
            elif option == "LOG_LEVEL_DEBUG":
                return 1
        return None

    @patch("logger.Logger.config.getint", __mock_get_level)
    @patch("logger.Logger.config.has_option", (lambda x, y: True
                                               if x == "LOGS"
                                               and (y == "LOG_LEVEL"
                                                    or y == "LOG_LEVEL_DEBUG")
                                               else False))
    @patch("logger.Logger.config", Mock())
    @patch("defaults.LOG_LEVEL", 4)
    @patch("defaults.LOG_LEVEL_DEBUG", 3)
    @patch("logger.DEBUG", False)
    def test_get_log_level(self):
        lvl = logger.Logger.get_log_level()
        self.assertEqual(lvl, 20)

        with patch("logger.Logger.config.getint", return_value=None):
            lvl = logger.Logger.get_log_level()
            self.assertEqual(lvl, 40)

        with patch("logger.DEBUG", True):
            lvl = logger.Logger.get_log_level()
            self.assertEqual(lvl, 10)

            with patch("logger.Logger.config.getint", return_value=None):
                lvl = logger.Logger.get_log_level()
                self.assertEqual(lvl, 30)

    @patch("os.makedirs")
    @patch("logger.Logger.config.get", return_value="/some/valid/path/config")
    @patch("logger.Logger.config.has_option", (lambda x, y: True
                                               if x == "LOGS"
                                               and y == "LOG_PATH"
                                               else False))
    @patch("logger.Logger.config", Mock())
    @patch("defaults.LOG_PATH", "/some/valid/path/default")
    def test_get_log_path(self, mock1, mock2):
        path = logger.Logger.get_log_path()
        self.assertEqual(path, "/some/valid/path/config")

        test_path = '~/some/valid/path/arg'
        with patch("logger.Logger.args", Mock(log=test_path)):
            path = logger.Logger.get_log_path()
            self.assertEqual(path, os.path.expanduser(test_path))

        with patch("logger.Logger.config.get", return_value=None):
            path = logger.Logger.get_log_path()
            self.assertEqual(path, "/some/valid/path/default")

    def __mock_get_format(section, option):
        if section == "LOGS":
            if option == "LOG_FORMAT":
                return "non debug format from config"
            elif option == "LOG_FORMAT_DEBUG":
                return "debug format from config"
        return None

    @patch("logger.Logger.config.get", __mock_get_format)
    @patch("logger.Logger.config.has_option", (lambda x, y: True
                                               if x == "LOGS"
                                               and (y == "LOG_FORMAT"
                                                    or y == "LOG_FORMAT_DEBUG")
                                               else False))
    @patch("logger.Logger.config", Mock())
    @patch("defaults.LOG_FORMAT", "non debug format from defaults")
    @patch("defaults.LOG_FORMAT_DEBUG", "debug format from defaults")
    @patch("logger.DEBUG", False)
    def test_get_log_format(self):
        log_format = logger.Logger.get_log_format()
        self.assertEqual(log_format, "non debug format from config")

        with patch("logger.Logger.config.get", return_value=None):
            log_format = logger.Logger.get_log_format()
            self.assertEqual(log_format, "non debug format from defaults")

        with patch("logger.DEBUG", True):
            log_format = logger.Logger.get_log_format()
            self.assertEqual(log_format, "debug format from config")

            with patch("logger.Logger.config.get", return_value=None):
                log_format = logger.Logger.get_log_format()
                self.assertEqual(log_format, "debug format from defaults")

    @patch("logger.Logger.config.get", (lambda x, y: "date format config"
                                        if x == "LOGS"
                                        and y == "LOG_DATE_FORMAT"
                                        else False))
    @patch("logger.Logger.config.has_option", (lambda x, y: True
                                               if x == "LOGS"
                                               and y == "LOG_DATE_FORMAT"
                                               else False))
    @patch("logger.Logger.config", Mock())
    @patch("defaults.LOG_DATE_FORMAT", "date format defaults")
    def test_get_log_format(self):
        log_format = logger.Logger.get_log_date_format()
        self.assertEqual(log_format, "date format config")

        with patch("logger.Logger.config.get", return_value=None):
            log_format = logger.Logger.get_log_date_format()
            self.assertEqual(log_format, "date format defaults")
