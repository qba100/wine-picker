import defaults
import unittest
from wine_picker import WinePicker
from mock import patch


class TestWinePicker(unittest.TestCase):

    def setUp(self):
        self.wine_picker = WinePicker()
        self.config_paths_backup = defaults.CONFIG_PATHS.copy()
        defaults.CONFIG_PATHS = []

    def tearDown(self):
        defaults.CONFIG_PATHS = self.config_paths_backup

    @patch('extended_configparser.ExtendedRawConfigParser.read')
    def test_load_config(self, mock_config_read):
        with self.assertRaisesRegex(Exception, 'Cannot load config file.'):
            self.wine_picker.load_config()

        defaults.CONFIG_PATHS = ['valid_path']
        mock_config_read.return_value = True
        try:
            self.wine_picker.load_config()
        except:
            self.fail("Encountered an unexpected exception.")

        defaults.CONFIG_PATHS = ['invalid_path']
        mock_config_read.return_value = False
        with self.assertRaisesRegex(Exception, 'Cannot load config file.'):
            self.wine_picker.load_config()

        defaults.CONFIG_PATHS.append("valid path")
        with patch('extended_configparser.ExtendedRawConfigParser.read',
                   side_effect=[False, True]):
            try:
                self.wine_picker.load_config()
            except:
                self.fail("Encountered an unexpected exception.")
