import configparser
import json
import logging


class ExtendedRawConfigParser(configparser.RawConfigParser):

    def getlist(self, section, key):
        config_value = self.get(section, key)
        try:
            logging.debug("Parse json: %s", config_value)
            return json.loads(config_value)
        except:
            message = "Cannot parse json value from config section: \"{}\", key: \"{}\"".format(
                section, key)
            logging.error(message)
            raise Exception(message)
