CONFIG_PATHS = [
    'config.ini',
    '~/.local/etc/wine-picker/config.ini',
    '/etc/wine-picker/config.ini',
]


LOG_PATH = '~/.local/share/wine-picker/wine-picker.log'
LOG_DATE_FORMAT = '%d-%m-%y %H:%M:%S'

LOG_FORMAT = '%(asctime)s [%(levelname)s] : %(message)s'
LOG_LEVEL = 2

LOG_FORMAT_DEBUG = '%(asctime)s {%(pathname)s:%(lineno)d} [%(levelname)s] : %(message)s'
LOG_LEVEL_DEBUG = 1
