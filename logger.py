import os
import logging
import defaults

DEBUG = os.environ.get("WINE_PICKER_DEBUG", False)


class Logger():

    config = None
    args = None
    MAX_LOG_LEVEL = int(logging.CRITICAL / 10)
    MIN_LOG_LEVEL = int(logging.DEBUG / 10)

    def __init__(self):
        pass

    @staticmethod
    def setup(config, args):

        Logger.config = config
        Logger.args = args

        logging.basicConfig(filename=Logger.get_log_path(), filemode='w',
                            format=Logger.get_log_format(),
                            datefmt=Logger.get_log_date_format(),
                            level=Logger.get_log_level())

    @staticmethod
    def get_log_level():

        if not DEBUG:
            value_from_config = (Logger.config.getint("LOGS", "LOG_LEVEL")
                                 if Logger.config is not None and
                                 Logger.config.has_option("LOGS", "LOG_LEVEL")
                                 else None)
        else:
            value_from_config = (Logger.config.getint("LOGS", "LOG_LEVEL_DEBUG")
                                 if Logger.config is not None
                                 and Logger.config.has_option("LOGS",
                                                              "LOG_LEVEL_DEBUG")
                                 else None)

        if (value_from_config is not None
                and value_from_config in range(Logger.MIN_LOG_LEVEL,
                                               Logger.MAX_LOG_LEVEL)):
            return value_from_config * 10
        else:
            if DEBUG:
                return defaults.LOG_LEVEL_DEBUG * 10
            else:
                return defaults.LOG_LEVEL * 10

    @staticmethod
    def get_log_path():
        if Logger.args is not None and Logger.args.log is not None:
            if isinstance(Logger.args.log, str):
                return os.path.expanduser(Logger.args.log)

        value_from_config = (Logger.config.get("LOGS", "LOG_PATH")
                             if Logger.config is not None
                             and Logger.config.has_option("LOGS", "LOG_PATH")
                             else None)

        if value_from_config is not None:
            log_path = os.path.expanduser(value_from_config)
        else:
            log_path = os.path.expanduser(defaults.LOG_PATH)

        if not os.path.exists(os.path.dirname(log_path)):
            os.makedirs(os.path.dirname(log_path), mode=0o744)

        return log_path

    @staticmethod
    def get_log_format():
        if not DEBUG:
            value_from_config = (Logger.config.get("LOGS", "LOG_FORMAT")
                                 if Logger.config is not None
                                 and Logger.config.has_option("LOGS",
                                                              "LOG_FORMAT")
                                 else None)

            if value_from_config is None:
                return defaults.LOG_FORMAT
            else:
                return value_from_config
        else:
            value_from_config = (Logger.config.get("LOGS", "LOG_FORMAT_DEBUG")
                                 if Logger.config is not None
                                 and Logger.config.has_option("LOGS",
                                                              "LOG_FORMAT_DEBUG")
                                 else None)

            if value_from_config is None:
                return defaults.LOG_FORMAT_DEBUG
            else:
                return value_from_config

    @staticmethod
    def get_log_date_format():
        value_from_config = (Logger.config.get("LOGS", "LOG_DATE_FORMAT")
                             if Logger.config is not None
                             and Logger.config.has_option("LOGS",
                                                          "LOG_DATE_FORMAT")
                             else None)

        if value_from_config is None:
            return defaults.LOG_DATE_FORMAT
        else:
            return value_from_config
