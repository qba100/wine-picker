#!/usr/bin/env python3

import argparse
import os
import sys
import errno
import wine_git_tags
import extended_configparser
import errno
import defaults
import logging
import validator_collection
import logger

DEBUG = os.environ.get("WINE_PICKER_DEBUG", False)


class WinePicker:

    args = None
    wine_tags = None
    config = None

    def __init__(self):
        pass

    def run(self):
        self.args = self.arguments_parser()
        try:
            self.load_config()
            self.configure_logging()
        except Exception as error:
            print("Error: %s" % error)
            sys.exit(error.errno)

        if self.args.show_versions:
            try:
                self.wine_tags = wine_git_tags.WineGitTags(
                    self.config.getlist("REPOS", "WINE_GIT_REPOS"))
                for value in self.wine_tags.get_tags():
                    print(value)
            except Exception as error:
                print("Error: %s" % error)
                sys.exit(errno.EIO)

    def arguments_parser(self):
        parser = argparse.ArgumentParser(
            description='Pick wine versions to run Windows applications')
        group = parser.add_mutually_exclusive_group(required=True)
        group.add_argument('--show-versions', action='store_true',
                           help='show aviable wine versions')
        parser.add_argument('--config', metavar='FILE', type=str,
                            help='Set path to config file. Non-existing file is ignored')
        parser.add_argument('--log', metavar='FILE', type=str,
                            help='Set path to log file.')

        args = parser.parse_args()

        return args

    def configure_logging(self):
        logger.Logger.setup(self.config, self.args)

    def load_config(self):
        if self.args is not None and self.args.config is not None:
            defaults.CONFIG_PATHS = [self.args.config] + defaults.CONFIG_PATHS

        readed = False
        self.config = extended_configparser.ExtendedRawConfigParser()
        for config_name in defaults.CONFIG_PATHS:
            if self.config.read(config_name):
                readed = True
                break
        if readed is False:
            raise Exception("Cannot load config file.")


if __name__ == "__main__":
    WinePicker().run()
