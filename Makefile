
.PHONY: run_unit_test

run_unit_test:
	python3 -m unittest discover -s tests/unittests -p 'test_*.py'
