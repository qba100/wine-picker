#!/usr/bin/env python3
import git


class ExtendedGitCmd(git.cmd.Git):

    @staticmethod
    def ls_remote(url_repo):
        remote_refs = {}
        g = git.cmd.Git()

        for ref in g.ls_remote(url_repo).split('\n'):
            hash_ref_list = ref.split('\t')
            remote_refs[hash_ref_list[1]] = hash_ref_list[0]

        return remote_refs
