#!/usr/bin/env python3
import re
import os
from validator_collection import validators, checkers
from extended_git import ExtendedGitCmd


class WineGitTags:

    def __init__(self, url_repo):
        self.url_repos = []

        if isinstance(url_repo, str):
            if len(url_repo) > 0:
                url_repo = [url_repo]

        if len(url_repo) == 0:
            raise Exception("Wine git repo not set")

        for url in url_repo:
            if checkers.is_url(url):
                self.url_repos.append(url)

        if len(self.url_repos) == 0:
            raise Exception('{} is not valid url'.format(" ".join(url_repo)))

    def get_tags(self):
        r1 = re.compile(r'^refs/tags/(.*\d)$')
        dict_with_tags = []

        for url in self.url_repos:
            try:
                dict_with_tags = ExtendedGitCmd.ls_remote(url)
                break
            except:
                continue

        if len(dict_with_tags) == 0:
            raise Exception('Download wine git tags failed')

        list_tags = [r1.search(key).group(1) for key, value in
                     dict_with_tags.items() if r1.match(key)]

        return list_tags
